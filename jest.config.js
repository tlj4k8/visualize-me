module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  moduleFileExtensions: ['js', 'vue'],
  transform: {
    '.*\\.(vue)$': 'vue-jest',
  },
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,jsx,vue}',
    '!**/node_modules/**',
    '!**/components/icons/**',
    '!**/tests/**',
  ],
  coverageReporters: [
    'html',
    'text-summary',
    'text',
  ],
  roots: ['<rootDir>/src/', '<rootDir>/tests/'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  testMatch: [
    '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/tests/*.(js|jsx|ts|tsx)',
  ],
  testURL: 'http://localhost/',
};
