import SvgIcon from 'vue-svgicon'; // eslint-disable-line
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

Vue.use(SvgIcon, {
  tagName: 'svgicon',
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
