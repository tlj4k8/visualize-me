/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'brightness_1-24px': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<circle pid="1" cx="12" cy="12" r="10"/>'
  }
})
