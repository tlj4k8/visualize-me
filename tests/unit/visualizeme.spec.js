import { shallowMount, createLocalVue } from '@vue/test-utils';
import * as d3 from 'd3';
import { downloadPng } from 'svg-crowbar';
import VisualizeMe from '../../src/components/VisualizeMe.vue';
import SvgIcon from 'vue-svgicon'; // eslint-disable-line

const localVue = createLocalVue();
localVue.use(SvgIcon, {
  tagName: 'svgicon',
});

const wrapper = shallowMount(VisualizeMe, {
  localVue,
  propsData: {
    name: 'Jane Doe',
  },
  methods: {
    createVis: jest.fn(),
  },
});

jest.mock('d3');
jest.mock('svg-crowbar', () => ({ downloadPng: jest.fn() }));

describe('VisualizeMe.vue', () => {
  describe('snapshot: visualizeMe', () => {
    it('matches snapshot', () => {
      expect(wrapper.element).toMatchSnapshot();
    });
  });
  describe('method: parseFileName(name)', () => {
    const fileName = wrapper.props().name;
    it('adds an underscore in place of a space', async () => {
      const result = wrapper.vm.parseFileName(fileName);
      expect(result).toEqual('Jane_Doe');
    });
  });
  describe('method: saveVisual()', () => {
    d3.select.mockReturnValue({ node: () => 'svg' });
    it('makes sure download is called with required svg and fileName', () => {
      wrapper.vm.saveVisual();
      expect(downloadPng).toHaveBeenCalled();
      expect(downloadPng).toHaveBeenCalledWith('svg', 'Jane_Doe');
    });
  });
  describe('method: calculateFontSize(r)', () => {
    const test = [
      { d: { r: 30 }, font: '12px' },
      { d: { r: 25 }, font: '10px' },
      { d: { r: 40 }, font: '12px' },
    ];
    test.forEach((t) => {
      it(`returns a font size of ${t.font} when circle svg radius is ${t.d.r}`, () => {
        const result = wrapper.vm.calculateFontSize(t.d);
        expect(result).toEqual(t.font);
      });
    });
  });
  describe('method: calculateStrokeWidth(d)', () => {
    const test = [
      { d: { data: { optIn: true } }, width: 20 },
      { d: { data: { optIn: false } }, width: 0 },
    ];
    test.forEach((t) => {
      it(`returns stroke width of ${t.width} when optIn is ${t.d.data.optIn}`, () => {
        const result = wrapper.vm.calculateStrokeWidth(t.d);
        expect(result).toEqual(t.width);
      });
    });
  });
  describe('method: parseLeafName(d)', () => {
    const test = [
      { d: { data: { group: 'Person', name: 'John' } }, newName: '' },
      { d: { data: { group: 'Functional/Technical Roles', name: 'Software Engineer' } }, newName: ['Software', 'Engineer'] },
      { d: { data: { group: 'Functional/Technical Roles', name: 'SF' } }, newName: ['SF'] },
    ];
    test.forEach((t) => {
      it('returns array of split names if name is not Person else it returns an empty string', () => {
        const result = wrapper.vm.parseLeafName(t.d);
        expect(result).toEqual(t.newName);
      });
    });
  });
});
