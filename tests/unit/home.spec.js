import { shallowMount } from '@vue/test-utils';
import fetchMock from 'fetch-mock';
import readXlsxFile from 'read-excel-file';
import Home from '../../src/views/Home.vue';
import mockData from '../mock/mockdata.json';
import asyncFetch from '../test_utils/async_fetch';

jest.mock('read-excel-file');
const wrapper = shallowMount(Home);

describe('Home.vue', () => {
  describe('snapshot: home', () => {
    it('matches snapshot', () => {
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  describe('method: filterChildren()', () => {
    const data = [
      { name: 'Software Engineer', value: 150 },
      { name: 'Data Engineer', value: 75 },
      { name: 'Capability Lead', value: 0 },
    ];
    it('returns data with value greater than 0', () => {
      const result = wrapper.vm.filterChildren(data).length;
      const expectedResult = data.filter(d => d.value > 0).length;
      expect(result).toEqual(expectedResult);
    });
  });

  describe('method: createDataVisualization()', () => {
    let home;
    beforeEach(() => {
      home = shallowMount(Home);
    });

    it('tests when index 0 is not person', async () => {
      home.vm.fetchPhoto = jest.fn();
      const children = [
        { name: 'Software Engineer', value: 150 },
        { name: 'Data Engineer', value: 75 },
      ];
      home.vm.filterChildren = jest.fn(() => children);
      home.setData({
        data: [
          {
            value: 150,
            photo: 'photo',
            group: 'Business Roles',
            mastery: null,
            name: 'Capability Lead',
            optIn: false,
            twoYearExp: false,
          },
        ],
        name: 'fruit',
        photo: 'photo',
        clicked: 0,
      });
      await home.vm.createDataVisualization();
      const expected = {
        name: 'fruit',
        photo: 'photo',
        value: 600,
        group: 'Person',
      };
      expect(home.vm.data[0]).toStrictEqual(expected);
      expect(home.vm.clicked).toEqual(1);
      expect(home.vm.formattedData.name).toEqual('fruit');
      expect(home.vm.formattedData.children).toEqual(children);
    });
    it('tests when index 0 is a person', async () => {
      home.vm.fetchPhoto = jest.fn();
      const children = [
        { name: 'Software Engineer', value: 300 },
        { name: 'Data Engineer', value: 150 },
      ];
      home.vm.filterChildren = jest.fn(() => children);
      home.setData({
        data: [
          {
            group: 'Person',
            name: 'Taylor',
            photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD',
            value: 600,
          },
        ],
        name: 'fruit',
        photo: 'photo',
        clicked: 0,
      });
      await home.vm.createDataVisualization();
      const expected = {
        name: 'fruit',
        photo: 'photo',
        value: 600,
        group: 'Person',
      };
      expect(home.vm.data[0]).toStrictEqual(expected);
      expect(home.vm.clicked).toEqual(1);
      expect(home.vm.formattedData.name).toEqual('fruit');
      expect(home.vm.formattedData.children).toEqual(children);
    });
  });

  describe('method: fetchPhoto()', () => {
    const { photoUrl, blobResponse } = mockData.fetchPhoto;
    let home;

    beforeEach(() => {
      home = shallowMount(Home);
    });

    it('returns photo from image url', async () => {
      fetchMock.get(photoUrl, { Response: JSON.stringify() });
      const response = await asyncFetch(photoUrl); // mocks url fetch

      home.vm.blobToDataURL = jest.fn(() => blobResponse); // returns the blob response
      await home.vm.fetchPhoto(response.url); // calls the fetchPhoto method with the fetched url
      expect(response.url).toEqual(photoUrl);
      expect(home.vm.photo).toEqual(blobResponse);
    });
  });

  describe('method: createValue(mastery)', () => {
    const test = [
      { mastery: 'novice', masteryLevel: 75 },
      { mastery: 'proficient', masteryLevel: 150 },
      { mastery: 'advanced', masteryLevel: 300 },
      { mastery: 'expert', masteryLevel: 600 },
      { mastery: null || 'none', masteryLevel: 0 },
    ];
    test.forEach((t) => {
      it(`returns correct bubble size of ${t.masteryLevel} for ${t.mastery}`, () => {
        const result = wrapper.vm.createValue(t.mastery);
        expect(result).toEqual(t.masteryLevel);
      });
    });
  });

  describe('method: createBool(word)', () => {
    const test = [
      { bool: true, word: 'yes' },
      { bool: false, word: 'flamingo' },
      { bool: false, word: null },
    ];
    test.forEach((t) => {
      it(`returns ${t.bool} if word is ${t.word}`, () => {
        const result = wrapper.vm.createBool(t.word);
        expect(result).toBe(t.bool);
      });
    });
  });

  describe('computed: disabled()', () => {
    let home;

    beforeEach(() => {
      home = shallowMount(Home);
    });

    const test = [
      {
        data: [{ stuff: 'stuff in here' }],
        name: 'name',
        photoUrl: 'photo',
        bool: false,
      },
      {
        data: [{ stuff: 'stuff in here' }],
        name: '',
        photoUrl: '',
        bool: true,
      },
      {
        data: [],
        name: 'name',
        photoUrl: '',
        bool: true,
      },
      {
        data: [],
        name: '',
        photoUrl: 'photo',
        bool: true,
      },
      {
        data: [],
        name: '',
        photoUrl: '',
        bool: true,
      },
    ];
    test.forEach((t) => {
      it(`returns ${t.bool} if data=${t.data.length > 0} or name=${t.name.length > 0} or photoUrl=${t.photoUrl.length > 0}`, () => {
        home.setData({ data: t.data, name: t.name, photoUrl: t.photoUrl });
        const result = home.vm.disabled;
        expect(result).toBe(t.bool);
      });
    });
  });

  describe('method: blobToDataURL(blob)', () => {
    it('reformats a json object to read to an encoded URL', async () => {
      const blob = new Blob(['Blob'], { type: 'image/jpeg' });
      const result = await wrapper.vm.blobToDataURL(blob);
      const blobUrl = 'data:image/jpeg;base64,QmxvYg==';
      expect(result).toEqual(blobUrl);
    });
  });

  describe('method: parseFile(e)', () => {
    let home;
    beforeEach(() => {
      home = shallowMount(Home);
    });
    const { rows } = mockData;
    it('resets data and clicked when no excel file is selected', () => {
      const e = { target: { files: [] } };
      home.setData({ clicked: 1, data: ['cat'] });
      home.vm.parseFile(e);
      expect(home.vm.clicked).toEqual(0);
      expect(home.vm.data).toHaveLength(0);
    });
    it('reformats the data when excel file is selected', async () => {
      const expected = [
        {
          name: 'Agilist',
          twoYearExp: true,
          mastery: 'Novice',
          optIn: false,
          value: 75,
          group: 'Functional Roles',
        },
        {
          name: 'Business Analyst',
          twoYearExp: true,
          mastery: 'Proficient',
          optIn: false,
          value: 150,
          group: 'Functional Roles',
        },
        {
          name: 'Business Architect',
          twoYearExp: false,
          mastery: null,
          optIn: false,
          value: 0,
          group: 'Functional Roles',
        },
      ];
      readXlsxFile.mockResolvedValue(rows);
      const e = { target: { files: ['file'] } };
      home.setData({ data: [] });
      await home.vm.parseFile(e);
      expect(home.vm.data).toEqual(expected);
    });
  });
});
